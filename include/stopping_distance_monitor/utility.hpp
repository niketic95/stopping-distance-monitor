/**
 * @file utility.hpp
 * @author Milan Djokic (Milan.Djokic@rt-rk.com)
 * @author Stefan Nicetin (Stefan.Nicetin@rt-rk.com)
 * @author Stefan Dupljanin (Stefan.Dupljanin@rt-rk.com)
 * @brief Common utility functions and calculations used by tests and nodes
 * @version 0.1
 * @date 2019-02-01
 *
 * @copyright Copyright (c) 2019
 *
 */

#ifndef __UTILITY_HPP__
#define __UTILITY_HPP__

#include "ros/ros.h"

#include "pcl_conversions/pcl_conversions.h"
#include "pcl_ros/transforms.h"

#include "pcl/filters/crop_box.h"

#include "message_filters/subscriber.h"
#include "message_filters/time_synchronizer.h"

#include "geometry_msgs/Vector3Stamped.h"
#include "sensor_msgs/PointCloud.h"
#include "visualization_msgs/Marker.h"

/**
 * @brief The region of interest structure
 * @detials The Region Of Interest (ROI) structure is used
 * to define the filter boundaries for lider coordinates
 */
struct ROI
{
    float lane_width; ///< The width of the ROI
    float x_min;      ///< The minimal point of the ROI
    float roi_length; ///< The length of the ROI
    float min_height; ///< The minimum height of the ROI
    float max_height; ///< The maximum height of the ROI
};

/**
 * @brief Filteres the region of interest by using a box filter defined in the
 * ROI structure
 *
 * @param output_points Input point cloud
 *
 * @param input_points Output filtered point cloud
 *
 * @param roi Region Of Interest to filter
 */
void filter_roi(pcl::PointCloud< pcl::PointXYZ >& output_points,
                pcl::PointCloud< pcl::PointXYZ >& input_points, ROI roi);

/**
 * @brief Calculate the distance the object would stop given its velocity and
 * acceleration/deceleration
 *
 * @param velocity Velocity of the object
 * @param acceleration Acceleration of the Object
 * @return float The stopping distance
 */
float calculate_stopping_distance(float velocity, float acceleration);

#endif // __UTILITY_HPP__