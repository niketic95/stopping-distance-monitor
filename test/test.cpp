#include <gtest/gtest.h>

#include "stopping_distance_monitor/utility.hpp"


TEST(FilterTest, AllPointsInROI)
{
    pcl::PointCloud<pcl::PointXYZ> output_points;
    pcl::PointCloud<pcl::PointXYZ> input_points; 
    ROI roi;

    roi.lane_width = 1;
    roi.x_min = 0;
    roi.roi_length = 5;
    roi.min_height = 0;
    roi.max_height = 2;

    input_points.push_back(pcl::PointXYZ(0, 0, 0));
    input_points.push_back(pcl::PointXYZ(1, 0, 0));
    input_points.push_back(pcl::PointXYZ(2, 0, 0));
    input_points.push_back(pcl::PointXYZ(3, 0, 0));
    input_points.push_back(pcl::PointXYZ(4, 0, 0));
    input_points.push_back(pcl::PointXYZ(4, 0, 0));
    input_points.push_back(pcl::PointXYZ(0, 0.4, 0));
    input_points.push_back(pcl::PointXYZ(0, -0.4, 0));
    input_points.push_back(pcl::PointXYZ(0, 0, 0));
    input_points.push_back(pcl::PointXYZ(0, 0, 1));

    EXPECT_NO_THROW(filter_roi(output_points, input_points, roi));

    EXPECT_EQ(10, output_points.size());

}

TEST(FilterTest, AllPointsOutOfROI)
{
    pcl::PointCloud<pcl::PointXYZ> output_points;
    pcl::PointCloud<pcl::PointXYZ> input_points; 
    ROI roi;

    roi.lane_width = 4;
    roi.x_min = 0;
    roi.roi_length = 10;
    roi.min_height = 0;
    roi.max_height = 5;

    input_points.push_back(pcl::PointXYZ(11, 0, 0));
    input_points.push_back(pcl::PointXYZ(12, 0, 0));
    input_points.push_back(pcl::PointXYZ(222, 0, 0));
    input_points.push_back(pcl::PointXYZ(33, 0, 0));
    input_points.push_back(pcl::PointXYZ(45, 0, 0));
    input_points.push_back(pcl::PointXYZ(91, 0, 0));
    input_points.push_back(pcl::PointXYZ(0, 2.5, 0));
    input_points.push_back(pcl::PointXYZ(0, -2.5, 0));
    input_points.push_back(pcl::PointXYZ(0, 0, 6));
    input_points.push_back(pcl::PointXYZ(0, 0, -3));

    EXPECT_NO_THROW(filter_roi(output_points, input_points, roi));

    EXPECT_EQ(0, output_points.size());

}

TEST(FilterTest, HalfPointsInROI)
{
    pcl::PointCloud<pcl::PointXYZ> output_points;
    pcl::PointCloud<pcl::PointXYZ> input_points; 
    ROI roi;

    roi.lane_width = 1;
    roi.x_min = 0;
    roi.roi_length = 2;
    roi.min_height = 0;
    roi.max_height = 1;

    input_points.push_back(pcl::PointXYZ(1, 0, 0));
    input_points.push_back(pcl::PointXYZ(1.5, 0, 0));
    input_points.push_back(pcl::PointXYZ(0.5, 0, 0));
    input_points.push_back(pcl::PointXYZ(0.2, 0, 0));
    input_points.push_back(pcl::PointXYZ(0, 0.5, 0));
    input_points.push_back(pcl::PointXYZ(5, 2, 0));
    input_points.push_back(pcl::PointXYZ(10, 1.0, 0));
    input_points.push_back(pcl::PointXYZ(20, -1.0, 0));
    input_points.push_back(pcl::PointXYZ(30, 0, 2));
    input_points.push_back(pcl::PointXYZ(40, 0, -2));

    EXPECT_NO_THROW(filter_roi(output_points, input_points, roi));

    EXPECT_EQ(5, output_points.size());
}

TEST(StoppingDistanceTest, ZeroVelocity)
{
    float velocity = 0.0;
    float acceleration = -1.0;

    float ret = calculate_stopping_distance(velocity, acceleration);

    EXPECT_EQ(0.0, ret);
}

TEST(StoppingDistanceTest, NonZeroVelocity1)
{
    float velocity = 10.0;
    float acceleration = -1.0;

    float ret = calculate_stopping_distance(velocity, acceleration);

    EXPECT_EQ(50.0, ret);
}

TEST(StoppingDistanceTest, NonZeroVelocity2)
{
    float velocity = 10.0;
    float acceleration = -10.0;

    float ret = calculate_stopping_distance(velocity, acceleration);

    EXPECT_EQ(5.0, ret);
}

TEST(StoppingDistanceTest, NonZeroVelocity3)
{
    float velocity = 10.0;
    float acceleration = -10.0;

    float ret = calculate_stopping_distance(velocity, acceleration);

    EXPECT_NE(0.0, ret);
}


int main(int argc, char  **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}