cmake_minimum_required(VERSION 2.8.3)
project(stopping_distance_monitor)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_compile_options(-Wall)


find_package(catkin REQUIRED COMPONENTS
  geometry_msgs
  roscpp
  rospy
  sensor_msgs
  pcl_conversions
  pcl_ros
)

find_package(GTest REQUIRED)

catkin_package()

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

add_executable(${PROJECT_NAME}_node src/stopping_distance_monitor_node.cpp src/utility.cpp)
add_executable(${PROJECT_NAME}_test test/test.cpp src/utility.cpp)

target_compile_options(${PROJECT_NAME}_node BEFORE PUBLIC -Werror)

target_link_libraries(
  ${PROJECT_NAME}_node
  ${catkin_LIBRARIES}
)

target_link_libraries(
  ${PROJECT_NAME}_test
  ${catkin_LIBRARIES}
  ${GTEST_LIBRARY}
  ${GTEST_MAIN_LIBRARIES}
  ${CMAKE_THREAD_LIBS_INIT}
)