/**
 * @file stopping_distance_monitor_node.cpp
 * @author Milan Djokic (Milan.Djokic@rt-rk.com)
 * @author Stefan Nicetin (Stefan.Nicetin@rt-rk.com)
 * @author Stefan Dupljanin (Stefan.Dupljanin@rt-rk.com)
 * @brief The stopping distance node.
 *
 * @detials Source file that implements the stopping distance monitor node
 * subscriber and publisher. The node recieves lidar data points in point cloud
 * as well as the velocity the car has and publishes line strips and visual
 * markers for teh stopping distance
 *
 * @version 0.1
 * @date 2019-02-01
 *
 * @copyright Copyright (c) 2019
 *
 */

#include <cmath>
#include <iomanip>
#include <sstream>

#include "ros/ros.h"

#include "pcl/filters/crop_box.h"
#include "pcl_conversions/pcl_conversions.h"
#include "pcl_ros/transforms.h"

#include "message_filters/subscriber.h"
#include "message_filters/time_synchronizer.h"

#include "geometry_msgs/Vector3Stamped.h"
#include "sensor_msgs/PointCloud2.h"
#include "visualization_msgs/Marker.h"

#include "stopping_distance_monitor/utility.hpp"

using namespace message_filters;

ROI roi;
float acceleration = -7.0;

ros::Publisher marker_pub;
geometry_msgs::Point min_geom_point;
geometry_msgs::Point max_geom_point;

std::string frame_id;

void pointCloudCallback(const sensor_msgs::PointCloud2::ConstPtr& msg,
                        const geometry_msgs::Vector3Stamped::ConstPtr& vel_msg)
{
    tf::StampedTransform transform;

    min_geom_point.x = 5.0;
    min_geom_point.y = 0;
    min_geom_point.z = 0;

    max_geom_point.x = roi.roi_length;
    max_geom_point.y = 0;
    max_geom_point.z = 0;

    transform.setOrigin({1.2, 0.0, 2.0});
    transform.setRotation({0.0, 0.0, 0.0, 1.0});

    visualization_msgs::Marker safe_distance_marker;

    safe_distance_marker.header.frame_id = frame_id.data();
    safe_distance_marker.header.stamp = ros::Time::now();
    safe_distance_marker.ns = "basic_shapes";
    safe_distance_marker.id = 0;
    safe_distance_marker.type = visualization_msgs::Marker::LINE_STRIP;
    safe_distance_marker.action = visualization_msgs::Marker::ADD;
    safe_distance_marker.lifetime = ros::Duration();
    safe_distance_marker.color.a = 1.0;
    safe_distance_marker.color.r = 0.5;
    safe_distance_marker.color.g = 1.0;
    safe_distance_marker.color.b = 0.5;
    safe_distance_marker.scale.x = 1.0;
    safe_distance_marker.pose.orientation.w = 1.0;

    pcl::PointCloud< pcl::PointXYZ > nesto;
    pcl::PointCloud< pcl::PointXYZ > transformed_points;
    pcl::fromROSMsg(*msg, nesto);

    pcl_ros::transformPointCloud(nesto, transformed_points, transform);

    filter_roi(nesto, transformed_points, roi);

    auto end_point =
        std::min_element(std::begin(nesto), std::end(nesto),
                         [](auto& a, auto& b) { return a.x < b.x; });

    if (end_point != std::end(nesto))
    {
        max_geom_point.x = end_point->x;
    }

    safe_distance_marker.points.push_back(min_geom_point);
    safe_distance_marker.points.push_back(max_geom_point);

    marker_pub.publish(safe_distance_marker);

    // STOPPING DISTANCE
    geometry_msgs::Point stopping_distance = min_geom_point;
    stopping_distance.x +=
        calculate_stopping_distance(vel_msg->vector.x, acceleration);

    visualization_msgs::Marker stopping_distance_marker;

    std::copy(std::begin(safe_distance_marker.points),
              std::end(safe_distance_marker.points),
              std::back_inserter(stopping_distance_marker.points));

    stopping_distance_marker.points[1].x = stopping_distance.x;
    stopping_distance_marker.points[0].z =
        stopping_distance_marker.points[1].z = 0.1;

    stopping_distance_marker.color.b = 1.0;
    stopping_distance_marker.color.a = 1.0;

    stopping_distance_marker.header.frame_id = frame_id.data();
    stopping_distance_marker.header.stamp = ros::Time::now();
    stopping_distance_marker.ns = "basic_shapes";
    stopping_distance_marker.id = 1;
    stopping_distance_marker.type = visualization_msgs::Marker::LINE_STRIP;
    stopping_distance_marker.action = visualization_msgs::Marker::ADD;
    stopping_distance_marker.lifetime = ros::Duration();
    stopping_distance_marker.scale.x = 1.0;
    stopping_distance_marker.pose.orientation.w = 1.0;

    marker_pub.publish(stopping_distance_marker);

    // TEXT BOX
    visualization_msgs::Marker text_marker;
    text_marker.header.frame_id = frame_id.data();
    text_marker.header.stamp = ros::Time::now();
    text_marker.ns = "basic_shapes";
    text_marker.id = 2;
    text_marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
    text_marker.lifetime = ros::Duration();
    text_marker.scale.x = 0.2;
    text_marker.scale.y = 0.2;
    text_marker.scale.z = 0.2;
    text_marker.pose.position.x = 0.0;
    text_marker.pose.position.y = 0.0;
    text_marker.pose.position.z = 1.0;
    text_marker.pose.orientation.w = 1.0;
    text_marker.color.r = 1.0;
    text_marker.color.g = 1.0;
    text_marker.color.b = 1.0;

    text_marker.color.a = 1.0;
    text_marker.action = visualization_msgs::Marker::ADD;

    std::stringstream ss;
    ss << "Current speed: " << vel_msg->vector.x << "m/s"
       << "\n"
       << "Stopping distance: " << std::setprecision(4)
       << calculate_stopping_distance(vel_msg->vector.x, acceleration) << " m"
       << std::endl;
    text_marker.text = ss.str().data();

    marker_pub.publish(text_marker);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "stopping_distance_monitor_node");

    ros::NodeHandle n;

    std::string input_point_topic;
    std::string input_velocity_topic;
    std::string output_marker_topic;

    n.getParam("/lane_width", roi.lane_width);
    n.getParam("/max_height", roi.max_height);
    n.getParam("/min_height", roi.min_height);
    n.getParam("/max_vehicle_dec", acceleration);
    n.getParam("/roi_length", roi.roi_length);

    n.getParam("/input_point_topic", input_point_topic);
    n.getParam("/input_velocity_topic", input_velocity_topic);
    n.getParam("/distance_markers", output_marker_topic);

    n.getParam("/coordinate_frames", frame_id);

    message_filters::Subscriber< sensor_msgs::PointCloud2 > lidar_sub(
        n, input_point_topic.data(), 1000);
    message_filters::Subscriber< geometry_msgs::Vector3Stamped > vel_sub(
        n, input_velocity_topic.data(), 1000);

    TimeSynchronizer< sensor_msgs::PointCloud2, geometry_msgs::Vector3Stamped >
        sync(lidar_sub, vel_sub, 1000);

    sync.registerCallback(boost::bind(&pointCloudCallback, _1, _2));

    marker_pub = n.advertise< visualization_msgs::Marker >(
        output_marker_topic.data(), 1000);

    roi.x_min = 5.0;

    ros::spin();

    return 0;
}
