/**
 * @file utility.cpp
 * @author Milan Djokic (Milan.Djokic@rt-rk.com)
 * @author Stefan Nicetin (Stefan.Nicetin@rt-rk.com)
 * @author Stefan Dupljanin (Stefan.Dupljanin@rt-rk.com)
 * @brief Common utility functions and calculations used by tests and nodes
 * @version 0.1
 * @date 2019-02-01
 *
 * @copyright Copyright (c) 2019
 *
 */

#include <cmath>

#include "stopping_distance_monitor/utility.hpp"

void filter_roi(pcl::PointCloud< pcl::PointXYZ >& output_points,
                pcl::PointCloud< pcl::PointXYZ >& input_points, ROI roi)
{
    pcl::CropBox< pcl::PointXYZ > boxFilter;
    boxFilter.setMin(
        Eigen::Vector4f(roi.x_min, -roi.lane_width / 2.0, roi.min_height, 0.0));
    boxFilter.setMax(Eigen::Vector4f(roi.roi_length, roi.lane_width / 2.0,
                                     roi.max_height, 0.0));
    boxFilter.setInputCloud(input_points.makeShared());
    boxFilter.filter(output_points);
}

float calculate_stopping_distance(float velocity, float acceleration)
{
    return std::pow(velocity, 2.0) / (2.0 * (acceleration)) * (-1.0);
}
